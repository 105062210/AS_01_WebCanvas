var canvas;
var context;

var paint = false;
var drawBuffer_X = new Array();
var drawBuffer_Y = new Array();
var clickDrag = new Array();
var startPoint = {
  x: 0,
  y: 0
};
var endPoint = {
  x: 0,
  y: 0
};
var color;
const PEN = 0;
const LINE = 1;
const CIRCLE = 2;
const TRIANGLE = 3;
const RECTANGLE = 4;
var draw_shape = 1;
var paint_shape;
var paint_size = 2 * 2;
var fill_or_border;
///////////////////////////////
var enter_text; //true false
var text_size; //px
var text_font;
var text_color = '#000000';
var text_content;//your words
let lastX = 0;
let lastY = 0;
var cursor;
var tempOnmouseup;
var tempOnmousemove;
var tempOnmousedown;
var drawText = false;
var drawImage = false;
var drawTriangle = false;
var imgToDraw;

const history = {
  redoList: [],
  undoList: [],
  saveState: function(canvas, list, keep_redo) {
    keep_redo = keep_redo || false;
    if (!keep_redo)
      this.redoList = [];
    // console.log('list: ' + list);
    // console.log('undoList: ', this.undoList);
    // console.log('newCanvas: ' + canvas);
    (list || this.undoList).push(canvas.toDataURL());
  },
  restoreState: function(canvas, pop, push) {
    if (pop.length) {
      this.saveState(canvas, push, true);
      var restore_state = pop.pop();
      // console.log('restore_state: ' + restore_state);
      var img = new Image();
      img.src = restore_state;
      // console.log(img);
      img.onload = function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
      }.bind(this)
    }
  },
  undo: function(canvas) {
    // console.log('undo');
    this.restoreState(canvas, this.undoList, this.redoList);
  },
  redo: function(canvas) {
    // console.log('redo');
    this.restoreState(canvas, this.redoList, this.undoList);
  }
};

window.onload = function() {
  canvas = document.getElementById("drawingCanvas");
  context = canvas.getContext("2d");
  canvas.onmousedown = startDrawing;
  canvas.onmousemove = Drawing;
  canvas.onmouseup = stopDrawing;
  context.lineCap = 'round';
  context.lineWidth = paint_size;
  context.strokeStyle = '#000000';
  context.fillStyle = '#000000';
  cursor = "pen";
};

function Undo() {
  history.undo(canvas);
}

function Redo() {
  history.redo(canvas);
}


function clearCanvas() { // Clear the canvas
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function changeColor(color) { // Change color
    text_color = color;
    context.strokeStyle = text_color;
    context.fillStyle = text_color;
}

function uploadImage(e) {
  var reader = new FileReader();
    reader.onload = function(event){
        imgToDraw = new Image();
        imgToDraw.src = event.target.result;
        console.log(imgToDraw);
    }
    reader.readAsDataURL(e.target.files[0]); 

}

function startToDrawImage() {
  document.getElementById("drawingCanvas").style.cursor = "url(CS-Blk-Cross.cur), default";
  history.saveState(canvas);  
  tempOnmousedown = canvas.onmousedown;
  tempOnmousemove = canvas.onmousemove;
  tempOnmouseup = canvas.onmouseup;
  canvas.onmousedown = drawingImage;
  canvas.onmousemove = null;
  canvas.onmouseup = null;
  drawImage = true; 
}

const drawingImage = function(e) {
  var x = e.clientX - e.target.offsetLeft;
  var y = e.clientY - e.target.offsetTop;
  context.drawImage(imgToDraw, x, y);
  canvas.onmousedown = tempOnmousedown;
  canvas.onmousemove = tempOnmousemove;
  canvas.onmouseup = tempOnmouseup;
  switch(cursor) {
    case "pen":
      document.getElementById("drawingCanvas").style.cursor = "url(PencilHandwriting.cur), default";
      break;
    case "shape":
      document.getElementById("drawingCanvas").style.cursor = "crosshair";
      break;
    case "eraser":
      document.getElementById("drawingCanvas").style.cursor = "url(kirby_stand.cur), default";
      break;
  }
}

function lineStyle(style) {
  switch(style) {
    case 1:
      context.lineCap = 'round';
      break;
    case 2:
      context.lineCap = 'square';
      break;
    case 3:
      drawTriangle = true;
      return;
  }
  drawTriangle = false;
}

function Eraser() {
  context.strokeStyle = '#ffffff';
  context.fillStyle = '#ffffff';
  document.getElementById("drawingCanvas").style.cursor = "url(kirby_stand.cur), default";
  cursor = "eraser";
}

function ChangePenSize(size) {
  context.lineWidth = size;
}

// Change Full or Border
function changeFillOrBorder(fillOrBorder) { // Change fill or border
    fill_or_border = fillOrBorder;
}

// Non-pen draw
const startDrawingShape = function(e) { // Pressed the left click and remember the start point
  startPoint.x = e.clientX - e.target.offsetLeft;
  startPoint.y = e.clientY - e.target.offsetTop;
  history.saveState(canvas);
}

const stopDrawingShape = function(e) { // Release the left click and draw the shape according to the start point and end point
  if (drawText) {
    drawText = false;
    return;
  }
  if (drawImage) {
    drawImage = false;
    return;
  }
  endPoint.x = e.clientX - e.target.offsetLeft;
  endPoint.y = e.clientY - e.target.offsetTop;
  context.strokeStyle = text_color;
  context.fillStyle = text_color;
  // Circle
  switch (draw_shape) {
    case 1: // Line
      context.moveTo(startPoint.x, startPoint.y);
      context.lineTo(endPoint.x, endPoint.y);
      context.stroke();
      break;
    case 2: // Circle
      var radius = Math.sqrt(Math.pow(startPoint.x - endPoint.x, 2) + Math.pow(startPoint.y - endPoint.y, 2));
      console.log('半徑: ' + radius);
      context.beginPath();
      context.arc(startPoint.x, startPoint.y, radius * 0.5, 0, Math.PI*2, true);
      context.closePath();
      if (!fill_or_border)
        context.stroke();
      else {
        context.fill();
      }
      break;
    case 3: // Triangle
      var endPoint_opposite = {
        x: 0,
        y: 0
      };
      endPoint_opposite.x = endPoint.x - 2 * (endPoint.x - startPoint.x);
      endPoint_opposite.y = endPoint.y;
      context.beginPath();
      context.moveTo(startPoint.x, startPoint.y);
      context.lineTo(endPoint.x, endPoint.y);
      context.lineTo(endPoint_opposite.x, endPoint_opposite.y);
      context.lineTo(startPoint.x, startPoint.y);
      context.closePath();
      if (fill_or_border)
        context.fill();
      else
        context.stroke();
      break;
    case 4: // Rectangle
      var topLeft = {
        x: 0,
        y: 0
      };
      topLeft.x = (startPoint.x < endPoint.x)? startPoint.x : endPoint.x;
      topLeft.y = (startPoint.y < endPoint.y)? startPoint.y : endPoint.y;

      if (fill_or_border) 
        context.fillRect(topLeft.x, topLeft.y, Math.abs(startPoint.x - endPoint.x), Math.abs(startPoint.y - endPoint.y));
      else {
        var bottomRight = {
          x: 0,
          y: 0
        };
        bottomRight.x = (startPoint.x > endPoint.x)? startPoint.x : endPoint.x;
        bottomRight.y = (startPoint.y > endPoint.y)? startPoint.y : endPoint.y;

        context.beginPath();
        context.moveTo(topLeft.x, topLeft.y);
        context.lineTo(bottomRight.x, topLeft.y);
        context.lineTo(bottomRight.x, bottomRight.y);
        context.lineTo(topLeft.x, bottomRight.y);
        context.lineTo(topLeft.x, topLeft.y);
        context.closePath();
        context.stroke();
      }
      break;    
    default:
      console.log('Nothing!');

  }
}

function f_draw_shape(e) { // Buttons to change the drawing shape
  switch (e.target.id) {
    case 'pen':
      draw_shape = PEN;
      break;
    case 'line':
      draw_shape = LINE;
      break;
    case 'circle':
      draw_shape = CIRCLE;
      break;
    case 'triangle':
      draw_shape = TRIANGLE;
      break;
    case 'square':
      draw_shape = RECTANGLE;
      break;
    default:
  }

  if (draw_shape != PEN) {
    document.getElementById("drawingCanvas").style.cursor = "crosshair";
    canvas.onmousedown = startDrawingShape;
    canvas.onmousemove = null;
    canvas.onmouseup = stopDrawingShape;
    cursor = "shape";
  } else {
    document.getElementById("drawingCanvas").style.cursor = "url(PencilHandwriting.cur), default";
    canvas.onmousedown = startDrawing;
    canvas.onmousemove = Drawing;
    canvas.onmouseup = stopDrawing;
    cursor = "pen";
  }
  context.strokeStyle = text_color;
  context.fillStyle = text_color;
}

// Pen draw
const startDrawing = function(e) { // Pressed the left click to begin to draw
    if (e.which == 1) { // Only left click affect
      paint = true;
      history.saveState(canvas);
      var x = e.clientX - e.target.offsetLeft;
      var y = e.clientY - e.target.offsetTop;
      [lastX, lastY] = [x, y];
      if (!drawTriangle) {
        context.beginPath();
        context.moveTo(lastX, lastY);
        context.lineTo(x, y);
        context.stroke();
      } 
      else {
        context.beginPath();
        context.moveTo(lastX, lastY - 3);
        context.lineTo(lastX + 3, lastY + 3*Math.sqrt(3));
        context.lineTo(lastX - 3, lastY + 3*Math.sqrt(3));
        context.moveTo(lastX + 3, lastY + 3*Math.sqrt(3));
        context.lineTo(lastX - 3, lastY + 3*Math.sqrt(3));
        context.closePath();
        context.fill();
      }
    }
};

const stopDrawing = function() { // Release the left click and stop drawing
    paint = false;
}

const Drawing = function(e) { // Hold the left click and draw
    if (paint) {
        var x = e.clientX - e.target.offsetLeft;
        var y = e.clientY - e.target.offsetTop;
        if (!drawTriangle) {
          context.beginPath();
          context.moveTo(lastX, lastY);
          context.lineTo(x, y);
          context.stroke();
        } 
        else {
          context.beginPath();
          context.moveTo(lastX, lastY - 3);
          context.lineTo(lastX + 3, lastY + 3*Math.sqrt(3));
          context.lineTo(lastX - 3, lastY + 3*Math.sqrt(3));
          context.moveTo(lastX + 3, lastY + 3*Math.sqrt(3));
          context.lineTo(lastX - 3, lastY + 3*Math.sqrt(3));
          context.closePath();
          context.fill();
        }
        [lastX, lastY] = [x, y];
    }
};
/////////////////////////////////////////////
function f_enter_words(e){ // To catch the content of text input and size input
    enter_text=true;

    console.log(document.getElementById("text_content").value);
    console.log(document.getElementById("text_size").value);

    text_size= document.getElementById("text_size").value;
    text_content=document.getElementById("text_content").value;
    context.font= text_size.toString() + "pt " + text_font;
    document.getElementById("drawingCanvas").style.cursor = "url(CS-Blk-Cross.cur), default";
    tempOnmousedown = canvas.onmousedown;
    tempOnmousemove = canvas.onmousemove;
    tempOnmouseup = canvas.onmouseup;
    canvas.onmousedown = drawingText;
    canvas.onmousemove = null;
    canvas.onmouseup = null;
    drawText = true;
}

function f_text_font(font){
    text_font= font;
}

function defaultText(e) {
  e.target.value = "教授我要100分";
}

const drawingText = function(e) {
  var x = e.clientX - e.target.offsetLeft;
  var y = e.clientY - e.target.offsetTop;
  context.fillText(text_content, x, y);
  canvas.onmousedown = tempOnmousedown;
  canvas.onmousemove = tempOnmousemove;
  canvas.onmouseup = tempOnmouseup;
  switch(cursor) {
    case "pen":
      document.getElementById("drawingCanvas").style.cursor = "url(PencilHandwriting.cur), default";
      break;
    case "shape":
      document.getElementById("drawingCanvas").style.cursor = "crosshair";
      break;
    case "eraser":
      document.getElementById("drawingCanvas").style.cursor = "url(kirby_stand.cur), default";
      break;
  }
}
