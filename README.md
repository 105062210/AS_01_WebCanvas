# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

105062210 READ_ME

(一)黃色區塊:

    1.ChangeColor:
        點擊選擇 筆刷 及 圖形 顏色

    2.ChangeSize:
        輸入數值(單位=px)調整 筆刷、橡皮擦 大小

(二)粉色區塊:

    1.Text:
        輸入欲生成之文字

    2.Size:
        輸入欲生成文字大小(px)

    3.ChangeFont:
        下拉式選單 提供三種字型選擇

    4.DrawTheseWords:
        按下此按鍵 再點擊畫布 將在點擊處生成文字

(三)藍色區塊:

    1.選擇檔案:
        點擊可選擇遇上傳之圖檔
        (會顯示已選檔案名稱，default顯示"未選擇任何檔案")

    2.DrawImage:
        按下此按鍵 再點擊畫布 將在點擊處生成圖檔
        欲再生成下一個同樣的圖檔 需再次點擊"DrawImage"

(四)按鍵組:

    1-1. Download:
        下載當前畫面

    1-2. ClearAll:
        清除畫面

    2-1. Undo:
        復原

    2-2. Redo:
        復原至復原前

    3-1. Pen:
        畫筆(自由繪圖)
        可藉由ChangeSize調整大小

    3-2. Eraser:
        橡皮擦
        可藉由ChangeSize調整大小

    4-1. Line:
        從滑鼠點擊處到放開右鍵處畫一直線
        可調顏色

    4-2. Circle:
        從滑鼠點擊處到放開右鍵處畫一圓形
        (連線長度為其直徑)
        可調顏色

    4-3. Rectangle:
        畫直線
        從滑鼠點擊處到放開右鍵處畫一矩形
        (連線為其對角線)
        可調顏色

    4-4. Triangle:
        畫直線
        從滑鼠點擊處到放開右鍵處畫一等腰三角形
        (連線為其一腰)
        可調顏色

    5-1. FullShape:
        選取時 4-1 ~ 4-4 圖形為實心

    5-2. BorderShape:
        選取時 4-1 ~ 4-4 圖形為空心

    6-1. RoundHead:
        圓型筆刷

    6-2. BlockHead:
        方形筆刷

    6-3. TriangleHead:
        三角形筆刷
        (這個可以拿來畫森林，很可愛)

